import { Component, OnInit, Input } from '@angular/core';
import { ShipsService } from 'src/app/business/ships.service';
import { StaticService } from 'src/app/static/static/static.service';
import { Proccess } from 'src/app/static/catcher/proccess.class';
import { CatcherService } from 'src/app/static/catcher/catcher.service';

@Component({
  selector: 'users-user-unit',
  templateUrl: './user-unit.component.html',
  styleUrls: ['./user-unit.component.scss']
})
export class UserUnitComponent implements OnInit {

  @Input() data:any;

  isCurrent = false;

  requestProccess:Proccess = {
    error: { }
  }

  currentShips:{ships:any[], requests:any} = {
    ships: [],
    requests: {}
  }

  constructor(
    private ships:ShipsService,
    private staticService:StaticService,
    private catcher:CatcherService
  ) { }

  ngOnInit(): void {
    if (this.data) {
      if (this.data.id == this.staticService.uid) {
        this.isCurrent = true;
        return;
      }
      this.getShip();
    }
  }

  async getShip() {
    this.currentShips = await this.ships.getRequestAndShip (
      this.staticService.uid,
      this.data.id
    );
    console.log (this.currentShips);
  }

  async removeRequest(request) {
    await this.ships.removeRequest (this.staticService.uid, this.data.id, request.id);
    this.currentShips.requests = undefined;
  }

  requestShip():void {
    this.requestProccess = this.catcher.unitCreate (async () => {
      console.log (await this.ships.createRequest (this.staticService.uid, this.data.id));
    });
    
  }

  async acceptRequest (request) {
    let response = await this.ships.createShip (this.staticService.uid, this.data.id, request.id);
    console.log (response);
  }
  userImage() {
    return this.data.tide_owner.post ? 
        '' :
        '../../assets/img/profile-none.png';
  }

}
