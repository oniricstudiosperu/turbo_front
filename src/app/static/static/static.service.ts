import { Injectable } from '@angular/core';
import { Observable, Subscription } from 'rxjs';

export enum AuthState {
  Undefined = "undefined",
  Logged = "logged",
  Unlogged = "unlogged",
  UpdatingFirst = "updatingfirst",
  Updating = "updating",
  Defined = "defined"
}

@Injectable({
  providedIn: 'root'
})
export class StaticService {

  status:AuthState = AuthState.Undefined;

  token:string;
  uid:string;

  user:any;
  validated:boolean = true;

  tokenChange:Observable<any>;
  private observers:any[];

  statusChange:Observable<any>;
  
  private statusObservers:any[];
  validateOperation:{
    id
  };

  setToken (token:string, uid:string) {
    if (!(this.token == token && this.uid == uid)) {
      this.token = token;
      this.uid = uid;

      if (typeof (token) == 'undefined' ) {
        localStorage.removeItem ('access_token');
      } else {
        localStorage.setItem ('access_token', token);
      }
      
      if (typeof (uid) == 'undefined') {
        localStorage.removeItem ('uid');
      } else {
        localStorage.setItem ('uid', uid);
      }
      this.observers.forEach(obs => obs.next({
        token: this.token,
        uid: this.uid
      }));
    }
  }

  reloadToken () {
    this.token = localStorage.getItem ('access_token');
    this.uid = localStorage.getItem ('uid');
  }
  
  constructor() {
    this.observers = [];
    this.statusObservers = [];
    this.tokenChange = new Observable((observer) => {
      this.observers.push (observer);
      return {
        unsubscribe() { }
      }
    });
    this.statusChange = new Observable ((observer) => {
      this.statusObservers.push (observer);
      return {
        unsubscribe () { }
      }
    });
    this.reloadToken();
  }

  private setStatus(status:AuthState) {
    this.status = status;
    this.statusObservers.forEach(obs => obs.next({
      status
    }));
  }

  async restartUser (funct: () => Promise<any>) {
    if (this.status == AuthState.Undefined) {
      this.setStatus (AuthState.UpdatingFirst);
    } else {
      this.setStatus (AuthState.Updating);
    }
    try {
      this.validated = true;
      this.setUser (await funct());
      this.validated = true;
    } catch (e) {
      this.setUser (undefined);
      this.validated = true;
      if (e.status != 401) {
        if (e.status == 400) {
          this.validateOperation = e.error.operation;
          this.validated = false;
        } else {
          throw e;
        }
      }
    }
    
  }

  private setUser (user) {
    this.user = user;
    this.setStatus (AuthState.Defined);
  }
}
