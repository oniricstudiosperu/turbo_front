import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { BusinessService } from 'src/app/business/business.service';
import { ShipsService } from 'src/app/business/ships.service';
import { StaticService } from 'src/app/static/static/static.service';

@Component({
  selector: 'unit-add-member',
  templateUrl: './add-member.component.html',
  styleUrls: ['./add-member.component.scss']
})
export class AddMemberComponent implements OnInit {

  control = new FormControl();
  streets: string[] = ['Champs-Élysées', 'Lombard Street', 'Abbey Road', 'Fifth Avenue'];
  filtered:any[];

  roles:any[] = [];

  constructor (
    private ships:ShipsService,
    private business:BusinessService,
    private staticService:StaticService
  ) { }

  search:any;

  ngOnInit() {
    let v = this;
    this.filtered = [];
    this.control.valueChanges.subscribe ({
      async next (value) {
        v.search = await v.ships.findInShips (v.staticService.uid, value);
        v.filtered = v.search.list;
        console.log (v.filtered)
      }
    });
    this.reloadRoles();
  }

  async reloadRoles() {
    this.roles = (await this.business.getRoles()).list;
    console.log (this.roles);
  }

}
