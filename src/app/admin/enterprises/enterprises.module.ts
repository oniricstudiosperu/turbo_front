import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EnterprisesRoutingModule } from './enterprises-routing.module';
import { EnterprisesComponent } from './enterprises.component';
import { CompsModule } from './comps/comps.module';

@NgModule({
  declarations: [EnterprisesComponent],
  imports: [
    CommonModule,
    EnterprisesRoutingModule,
    CompsModule
  ]
})
export class EnterprisesModule { }
