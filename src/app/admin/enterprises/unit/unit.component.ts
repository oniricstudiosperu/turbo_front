import { Component, OnInit } from '@angular/core';
import { BusinessService } from 'src/app/business/business.service';
import { ActivatedRoute } from '@angular/router';
import { UnitService } from './unit.service';

@Component({
  selector: 'app-unit',
  templateUrl: './unit.component.html',
  styleUrls: ['./unit.component.scss']
})
export class UnitComponent implements OnInit {

  id:number = null;
  data:any;
  constructor(
    private business:BusinessService,
    private activatedRoute:ActivatedRoute,
    private unit:UnitService
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(async (params) => {
      this.id = params.id;
      this.unit.setCurrentId (this.id);
      await this.reload();
    });
  }
  
  async reload() {
    if (this.id === null) {
      this.data = await this.business.getEnterprise (this.id);
      console.log (this.data);
      return;
    }
  }

}
