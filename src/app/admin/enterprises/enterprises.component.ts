import { Component, OnInit } from '@angular/core';
import { BusinessService } from 'src/app/business/business.service';
import { StaticService, AuthState } from 'src/app/static/static/static.service';
import { RestService } from 'src/app/static/rest/rest.service';

@Component({
  selector: 'app-enterprises',
  templateUrl: './enterprises.component.html',
  styleUrls: ['./enterprises.component.scss']
})
export class EnterprisesComponent implements OnInit {

  constructor (
    private business:BusinessService,
    private staticService:StaticService,
    private rest:RestService
  ) { }

  enterprises:any[];

  ngOnInit(): void {
    this.enterprises = [];
    this.staticService.statusChange.subscribe ({
      next: () => {
        this.reload();
      }
    });
    this.reload();
  }

  async relationship() {
    console.log (await this.rest.lget ('user/' + this.staticService.uid + '/ships/85'));
  }

  async reload() {
    if (this.staticService.status != AuthState.Defined ||
        !this.staticService.token) {
      return;
    }
    let result = await this.business.getMyEnterprises();
    
    this.enterprises = result.list;
    //this.relationship();
  }
}
