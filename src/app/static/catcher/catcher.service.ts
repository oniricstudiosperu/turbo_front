import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ProccessState, Proccess, LoadProccess } from './proccess.class';

@Injectable({
  providedIn: 'root'
})
export class CatcherService {

  constructor(
    private _snackBar: MatSnackBar
  ) { }

  setErrors (errors, form:FormGroup) {
    if (form) {
      Object.keys(errors).forEach(prop => {
        const formControl = form.get(prop);
        if (formControl && errors[prop]) {
          // activate the error message
          formControl.setErrors({
            serverError: errors[prop]
          });
        }
      });
    }
  }

  showToast (message:string, className:string) {
    this._snackBar.open (message, '', {
      panelClass: className,
      duration: 3 * 1000
    });
  }

  unitLoad (funct:() => Promise<any>, initial?:LoadProccess):LoadProccess {
    let turn = new LoadProccess();
    funct()
    .then ((res) => {
      turn.state = ProccessState.Success;
    })
    .catch ((res) => {
      turn.state = ProccessState.Failed;
      if (res.status == 404) {
        turn.state = ProccessState.NotFound;
      }
      if (res.error.error) {
        turn.error = res.error.error;
      } else {
        turn.error = res.error;
      }
    })
    return turn;
  }

  /**
   * 
   * @param funct acción que se realizará
   */
  unitCreate (funct:() => Promise<any>, initial?:Proccess):Proccess {
    let turn = new Proccess();

    funct()
    .then ((res) => {
      turn.state = ProccessState.Success;
      if (turn.successToast) {
        this.showToast('La operación se realizó correctamente', 'bg-success');
      }
      turn.form?.reset();
    })
    .catch ((res) => {
      turn.state = ProccessState.Failed;
      if (res.error.error) {
        turn.error = res.error.error;
      } else {
        turn.error = res.error;
      }
      if (res.status == 400) {
        this.setErrors (turn.error, turn.form);
      } else {
        if (turn.failToast) {
          this.showToast(turn.error, 'bg-danger');
        }
      }
      
    });
    if (initial) {
      for (let i in initial) {
        turn[i] = initial[i];
      }
    }
    
    return turn;
  }

}
