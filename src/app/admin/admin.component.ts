import { Component, OnInit } from '@angular/core';
import { AuthService } from '../static/auth/auth.service';
import { StaticService, AuthState } from '../static/static/static.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  authStates:any;
  subscription:Subscription;
  constructor(
    private auth:AuthService,
    public staticService:StaticService
  ) {
    this.authStates = AuthState;
  }

  ngOnInit(): void {
    this.subscription = this.staticService.tokenChange.subscribe ({
      next: () => {
        console.log ('here');
      }
    });
    this.reload();
  }

  onDestroy():void {
    this.subscription.unsubscribe();
  }

  reload () {
    
  }

}
