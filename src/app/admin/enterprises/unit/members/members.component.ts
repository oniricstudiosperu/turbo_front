import { Component, OnInit } from '@angular/core';
import { BusinessService } from 'src/app/business/business.service';
import { UnitService } from '../unit.service';
import { CatcherService } from 'src/app/static/catcher/catcher.service';
import { ProccessState } from 'src/app/static/catcher/proccess.class';

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.scss']
})
export class MembersComponent implements OnInit {

  id:number;
  list:any;
  proccess:any = {};
  proccessState = ProccessState;
  constructor(
    private business:BusinessService,
    private unit:UnitService,
    private catcher:CatcherService,
  ) { }

  ngOnInit(): void {
    this.id = this.unit.currentId;
    this.reload();
  }

  reload() {
    this.proccess = this.catcher.unitLoad (async () => {
      this.list = (await this.business.getEnterprise (this.id)).enterpriseRoleUsers;
      console.log (this.list);
      return this.list;
    })
    
  }
}
