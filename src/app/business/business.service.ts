import { Injectable } from '@angular/core';
import { RestService } from '../static/rest/rest.service';
import { StaticService } from '../static/static/static.service';

@Injectable({
  providedIn: 'root'
})
export class BusinessService {

  constructor(
    private rest:RestService,
    private staticService:StaticService
  ) { }

  async getMyEnterprises () {
    return await this.rest.lget ('user/'
      + this.staticService.user.id + '/enterprises', {});
  }

  async getRoles () {
    return await this.rest.lget ('enterprise-roles', {});
  }

  async createEnterprise (body) {
    return await this.rest.lpost ('user/' + this.staticService.user.id + '/enterprises', body)
  }

  async getEnterprise (id) {
    return await this.rest.lget ('enterprises/' + id, { support: 'members' });
  }

  async updateEnterprise (id, data) {
    return await this.rest.lput ('enterprises/' + id, data);
  }

  async removeEnterprise (id) {
    return await this.rest.ldelete ('enterprises/' + id);
  }

}
