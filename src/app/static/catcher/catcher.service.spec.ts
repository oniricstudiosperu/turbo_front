import { TestBed } from '@angular/core/testing';

import { CatcherService } from './catcher.service';

describe('CatcherService', () => {
  let service: CatcherService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CatcherService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
