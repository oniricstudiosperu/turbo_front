import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { UsersComponent } from './users.component';
import { BusinessModule } from 'src/app/business/business.module';
import { CompsModule } from './comps/comps.module';


@NgModule({
  declarations: [UsersComponent],
  imports: [
    CommonModule,
    UsersRoutingModule,
    BusinessModule,
    CompsModule
  ]
})
export class UsersModule { }
