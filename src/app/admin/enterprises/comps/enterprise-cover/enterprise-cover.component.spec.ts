import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnterpriseCoverComponent } from './enterprise-cover.component';

describe('EnterpriseCoverComponent', () => {
  let component: EnterpriseCoverComponent;
  let fixture: ComponentFixture<EnterpriseCoverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnterpriseCoverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnterpriseCoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
