import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BusinessService } from 'src/app/business/business.service';
import { CatcherService } from 'src/app/static/catcher/catcher.service';

@Component({
  selector: 'app-enterprise-integra',
  templateUrl: './enterprise-integra.component.html',
  styleUrls: ['./enterprise-integra.component.scss']
})
export class EnterpriseIntegraComponent implements OnInit {
  
  @Input('data') data;
  @Output('reload') parentReload:EventEmitter<any> = new EventEmitter();

  isValid:boolean;
  isDeleted:boolean;

  constructor(
    private business:BusinessService,
    private catcher:CatcherService
  ) { }

  ngOnInit(): void {
    this.isValid = !(!this.data && !this.data.id);
    this.isDeleted = false;
    this.reload();
  }

  async reload() {
    if (!this.isValid || this.isDeleted) return;
    await this.business.getEnterprise (this.data.id);
  }

  async remove() {
    if (!this.isValid || this.isDeleted) return;
    let proccess = this.catcher.unitCreate (async () => {
      this.business.removeEnterprise (this.data.id);
      this.isDeleted = true;
      this.parentReload.emit('deletion');
    })
    proccess.successToast = true;
  }

}
