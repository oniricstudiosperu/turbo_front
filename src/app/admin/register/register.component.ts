import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ConfirmedValidator } from './validator';
import { CatcherService } from 'src/app/static/catcher/catcher.service';
import { AuthService } from 'src/app/static/auth/auth.service';
import { StaticService } from 'src/app/static/static/static.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registerForm:FormGroup;
  proccess:any = {
    error: {}
  };
  errors: {};
  constructor (
    private builder:FormBuilder,
    private auth:AuthService,
    private catcher:CatcherService,
    private staticService:StaticService) { }

  ngOnInit(): void {
    this.registerForm = this.builder.group ({
      firstname: [ 'Jesús Loel', Validators.required ],
      lastname: [ 'Curi Luque', Validators.required ],
      account: [ 'jesuscuri13@gmail.com', Validators.required ],
      password: [ 'Conciencia134', Validators.required ],
      confirm_password: [ 'Conciencia134', Validators.required ],
      birthdate: [ new Date(), Validators.required ]
    },
    { 
      validator: ConfirmedValidator('password', 'confirm_password')
    });
  }

  register():void {
    if (this.registerForm.status != 'VALID') {
      return;
    }
    this.proccess = this.catcher.unitCreate(async () => {
      let response = await this.auth.register (this.registerForm.value);
      
      // Al registrarse, se guarda el código de la operación de validación
      this.staticService.validateOperation = response.operation;
      return response;
    });

    this.proccess.form = this.registerForm;    
    
  }

}
