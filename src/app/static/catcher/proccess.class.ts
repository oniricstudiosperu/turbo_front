import { FormGroup } from '@angular/forms';


export enum ProccessState {
    Sent = "sent",
    Success = "success",
    Failed = "failed",
    NotFound = 'notfound'
}

export class Proccess {
    
    state?:ProccessState = ProccessState.Sent;
    error?:any = { };
    promise?:Promise<any> = null;
    form?:FormGroup = null;
    successToast? :boolean = false;
    failToast?:boolean = false;
}

export class LoadProccess {
    state?:ProccessState = ProccessState.Sent;
    error?:any = { };
}