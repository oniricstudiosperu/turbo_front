import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UnitService {

  current:any;
  currentId:number;

  constructor() { }

  setCurrentId (id) {
    this.currentId = id;
  }
}
