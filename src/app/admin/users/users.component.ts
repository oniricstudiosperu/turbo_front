import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/business/users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  users:any;

  constructor (
    private _users:UsersService
  ) { }

  ngOnInit(): void {
    this.reload();
  }

  async reload() {
    this.users = await this._users.getUsers();
    console.log (this.users);
  }
}
