import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CatcherService } from 'src/app/static/catcher/catcher.service';
import { BusinessService } from 'src/app/business/business.service';
import { Router } from '@angular/router';
import { Proccess } from 'src/app/static/catcher/proccess.class';

@Component({
  selector: 'app-create-form',
  templateUrl: './create-form.component.html',
  styleUrls: ['./create-form.component.scss']
})
export class CreateFormComponent implements OnInit {

  constructor(
    private builder:FormBuilder,
    private catcher:CatcherService,
    private business:BusinessService,
    private router:Router
  ) { }

  proccess:Proccess = {
    error: {}
  };
  enterpriseForm:FormGroup;

  ngOnInit(): void {
    this.enterpriseForm = this.builder.group({
      name: ['', Validators.required],
      description: ['', Validators.required]
    })
  }
  resetForm() {
    this.enterpriseForm.reset();
  }
  
  create() {
    if (this.enterpriseForm.status != 'VALID') {
      return;
    }
    this.proccess = this.catcher.unitCreate(async () => {
      let enterprise = await this.business.createEnterprise (this.enterpriseForm.value);
      this.router.navigate(['enterprises/' + enterprise.id]);
      return enterprise;
    }, {
      form: this.enterpriseForm,
      failToast: true
    });
  }
}
