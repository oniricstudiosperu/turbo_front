import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnterpriseSubmenuComponent } from './enterprise-submenu.component';

describe('EnterpriseSubmenuComponent', () => {
  let component: EnterpriseSubmenuComponent;
  let fixture: ComponentFixture<EnterpriseSubmenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnterpriseSubmenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnterpriseSubmenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
