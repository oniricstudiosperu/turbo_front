import { Injectable } from '@angular/core';
import { RestService } from '../static/rest/rest.service';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(
    private rest:RestService
  ) { }

  async getUsers() {
    return await this.rest.lget ('user');
  } 

}
