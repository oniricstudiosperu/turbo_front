import { Injectable } from '@angular/core';
import { StaticService } from '../static/static.service';
import { RestService } from '../rest/rest.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor (
    private staticService:StaticService,
    private rest:RestService) { }

  async register(values) {
    let response:any = await this.rest.post ('register', values);
    this.staticService.setToken (response.access_token, response.uid);
    return response;
  }

  async login(values) {
    let response:any = await this.rest.post ('session', values);
    this.staticService.setToken (response.access_token, response.uid);
    return response;
  }

  async me () {
    return await this.rest.lget ('user/me');
  }

  async reloadUser () {
    this.staticService.restartUser (async () => {
      try {
        return await this.me();
      } catch (e) {
        throw e;
      }
      
    })
  }

  async validate (op:string, data) {
    return await this.rest.lpost ('user/me/validate/' + op, data);
  }

}
