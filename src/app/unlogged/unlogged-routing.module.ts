import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UnloggedComponent } from './unlogged.component';

const routes: Routes = [
  { path: '', component: UnloggedComponent,
    children: [
      { path: 'login', loadChildren: () => import('./login/login.module').then(m => m.LoginModule) },    
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UnloggedRoutingModule { }
