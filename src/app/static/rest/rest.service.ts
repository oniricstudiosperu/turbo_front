import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StaticService } from '../static/static.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(private client:HttpClient, private staticService:StaticService) { }

  api = environment.api;

  async get (endpoint:string, options?:any) {
    this.client.get (endpoint, options);
  }

  async post (endpoint:string, data?:any):Promise<any> {
    return await this.client.post (this.api + endpoint, data).toPromise();
  }
  headers () {
    let headers:any = {};
    if (this.staticService.token) {
      headers.access_token = this.staticService.token + '';
    }
    if (this.staticService.uid) {
      headers.uid = this.staticService.uid + '';
    }
    return headers;
  }

  async ldelete (endpoint:string):Promise<any> {
    let headers = this.headers();
    try {
      return await this.client.delete (this.api + endpoint, { headers }).toPromise();
    } catch (e) {
      if (e.status == 401) {
        this.staticService.setToken (undefined, undefined);
      }
      throw e;
    }
  }

  async lpost (endpoint:string, body?:any):Promise<any> {
    body = body || {};
    let headers = this.headers();
    try {
      return await this.client.post (this.api + endpoint, body, { headers }).toPromise();
    } catch (e) {
      if (e.status == 401) {
        this.staticService.setToken (undefined, undefined);
      }
      throw e;
    }
  }

  async lput (endpoint:string, body?:any):Promise<any> {
    body = body || {};
    let headers = this.headers();
    try {
      return await this.client.put (this.api + endpoint, body, { headers }).toPromise();
    } catch (e) {
      if (e.status == 401) {
        this.staticService.setToken (undefined, undefined);
      }
      throw e;
    }
  }

  async lget (endpoint:string, params?:any):Promise<any> {
    params = params || {};
    let headers:any = {};
    headers = this.headers();
    try {
      return await this.client.get (this.api + endpoint, { headers, params }).toPromise();
    } catch (e) {
      if (e.status == 401) {
        console.log (headers);
        console.log (this.staticService.token, this.staticService.uid)
        this.staticService.setToken (undefined, undefined);
      }
      throw e;
    }
    
  }
}
