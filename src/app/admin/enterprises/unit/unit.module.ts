import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UnitRoutingModule } from './unit-routing.module';
import { UnitComponent } from './unit.component';
import { CompsModule } from '../comps/comps.module';
import { UnitService } from './unit.service';


@NgModule({
  declarations: [UnitComponent],
  imports: [
    CommonModule,
    CompsModule,
    UnitRoutingModule
  ],
  providers: [
    UnitService
  ]
})
export class UnitModule { }
