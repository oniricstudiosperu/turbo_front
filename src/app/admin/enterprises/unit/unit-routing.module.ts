import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UnitComponent } from './unit.component';

const routes: Routes = [
  {
    path: '', component: UnitComponent,
    children: [
      { path: '', loadChildren: () => import('./home/home.module').then(m => m.HomeModule) },
      { path: 'products', loadChildren: () => import('./products/products.module').then(m => m.ProductsModule) },
      { path: 'info', loadChildren: () => import('./info/info.module').then(m => m.InfoModule) },
      { path: 'members', loadChildren: () => import('./members/members.module').then(m => m.MembersModule) }
    ]
  },
  
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UnitRoutingModule { }
