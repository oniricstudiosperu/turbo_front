import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { MatSidenav } from '@angular/material/sidenav';
import { StaticService, AuthState } from 'src/app/static/static/static.service';
import { RestService } from 'src/app/static/rest/rest.service';
import { AuthService } from 'src/app/static/auth/auth.service';
import { CatcherService } from 'src/app/static/catcher/catcher.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  @ViewChild('sidenav') sidenav: MatSidenav;

  reason = '';

  close(reason: string) {
    this.reason = reason;
    this.sidenav.close();
  }

  menu:any[];
  user;
  
  loginForm:FormGroup;
  subscription:Subscription;
  authStates:any;
  proccessLogin:any = {
    error: {}
  };

  constructor (
    public staticService:StaticService,
    private rest:RestService,
    private auth:AuthService,
    private catcher:CatcherService,
    private builder:FormBuilder,
  ) {
    this.authStates = AuthState;

    this.loginForm = this.builder.group ({
      account: [ 'test@test.test', Validators.required ],
      password: [ 'Caceria777', Validators.required ],
    });

  }

  login() {
    if (this.loginForm.status != 'VALID') {
      return;
    }
    this.proccessLogin = this.catcher.unitCreate(async () => {
      let response = await this.auth.login (this.loginForm.value);
      return response;
    });

    this.proccessLogin.form = this.loginForm;
    console.log (this.loginForm.value);
  }

  public checkError = (controlName: string, errorName: string) => {
    let control = this.loginForm.controls[controlName];
    if (control.invalid && (control.dirty || control.touched)) {
      return control.hasError(errorName);
    }
    return false;
  }

  public checkInvalid = (controlName: string) => {
    let control = this.loginForm.controls[controlName];
    return control.invalid && (control.dirty || control.touched);
  }

  openMain() {
    console.log ('here');
  }

  closeSession() {
    this.staticService.setToken(undefined, undefined);
  }

  ngOnInit() {
    this.subscription = this.staticService.tokenChange.subscribe ({
      next: (token) => {
        this.reloadMenu();
      }
    });
    this.reloadMenu();
  }

  async reloadMenu() {
    this.menu = await this.rest.lget ('menu');
    this.user = await this.auth.reloadUser();
  }

  onDestroy():void {
    this.subscription.unsubscribe();
  }

}
