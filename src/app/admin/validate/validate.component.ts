import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/static/auth/auth.service';
import { StaticService } from 'src/app/static/static/static.service';
import { CatcherService } from 'src/app/static/catcher/catcher.service';

@Component({
  selector: 'app-validate',
  templateUrl: './validate.component.html',
  styleUrls: ['./validate.component.scss']
})
export class ValidateComponent implements OnInit {

  operation:any;
  constructor(
    private builder:FormBuilder,
    private auth:AuthService,
    private staticService:StaticService,
    private catcher:CatcherService
  ) {
    this.validateForm = this.builder.group ({
      code: [ '', Validators.required ]
    });
  }
  validateForm:FormGroup;
  proccess:any = {
    error: {}
  };

  ngOnInit(): void {
    this.operation = this.staticService.validateOperation;
  }

  validate () {
    if (!this.staticService.validateOperation) {
      return;
    }
    this.proccess = this.catcher.unitCreate(async () => {
      let response = await this.auth.validate ( this.staticService.validateOperation.id, this.validateForm.value );
      if (response.success) {
        this.auth.reloadUser();
      }
      return response;
    });
    this.proccess.form = this.validateForm;
    
  }
}
