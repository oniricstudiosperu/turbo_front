import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShipsService } from './ships.service';
import { BusinessService } from './business.service';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [BusinessService, ShipsService]
})
export class BusinessModule { }
