import { Component, OnInit, Input } from '@angular/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'unit-member',
  templateUrl: './member.component.html',
  styleUrls: ['./member.component.scss']
})
export class MemberComponent implements OnInit {
  api = environment.api;
  
  @Input()
  data: {
    user:any;
    role:any;
  }
  

  constructor() { }

  ngOnInit(): void {
  }

  userImage() {
    return this.data.user.tide_owner.post ? 
        '' :
        '../../assets/img/profile-none.png';
  }

}
