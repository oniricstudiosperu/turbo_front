import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EnterprisesComponent } from './enterprises.component';

const routes: Routes = [
  { path: '', component: EnterprisesComponent },
  { path: 'create', loadChildren: () => import('./create/create.module').then(m => m.CreateModule) },
  { path: ':id', loadChildren: () => import('./unit/unit.module').then(m => m.UnitModule) }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EnterprisesRoutingModule { }
