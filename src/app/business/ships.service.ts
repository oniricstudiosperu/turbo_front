import { Injectable } from '@angular/core';
import { RestService } from '../static/rest/rest.service';

@Injectable({
  providedIn: 'root'
})
export class ShipsService {

  constructor(
    private rest:RestService
  ) { }

  async findInShips (userId:number|string, query:string) {
    return await this.rest.lget ('user/' + userId + '/ships/', { q: query })
  }

  async getRequestAndShip (userIdFrom:number|string, userIdTo:number|string) {
    let ships = await this.rest.lget ('user/' + userIdFrom + '/ships/' + userIdTo);
    let requests = await this.rest.lget ('user/' + userIdFrom + '/requests/' + userIdTo);
    return {
      ships,
      requests
    };
  }

  async createShip (from, to, req) {
    return await this.rest.lpost ('user/' + from + '/ships/' + to + '/' + req, {});
  }

  async createRequest (from:number|string, to:number|string) {
    return await this.rest.lpost ('user/' + from + '/requests/' + to, {});
  }

  async removeRequest (from, to, req) {
    return await this.rest.ldelete ('user/' + from + '/requests/' + to + '/' + req);
  }

}
