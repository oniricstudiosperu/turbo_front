import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnterpriseIntegraComponent } from './enterprise-integra.component';

describe('EnterpriseIntegraComponent', () => {
  let component: EnterpriseIntegraComponent;
  let fixture: ComponentFixture<EnterpriseIntegraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnterpriseIntegraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnterpriseIntegraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
