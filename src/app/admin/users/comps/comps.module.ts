import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserUnitComponent } from './user-unit/user-unit.component';



@NgModule({
  declarations: [UserUnitComponent],
  imports: [
    CommonModule
  ],
  exports: [UserUnitComponent]
})
export class CompsModule { }
