import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BusinessService } from 'src/app/business/business.service';
import { UnitService } from '../unit.service';
import { CatcherService } from 'src/app/static/catcher/catcher.service';
import { Proccess } from 'src/app/static/catcher/proccess.class';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss']
})
export class InfoComponent implements OnInit {

  properties = [
    {
      title: 'Nombre',
      name: 'name',
      type: 'string',
      placeholder: 'Ingresa el nombre',
      editting: false,
      proccess: Proccess
    },
    {
      title: 'Descripción',
      name: 'description',
      placeholder: 'Ingresa la descripción',
      type: 'string',
      editting: false,
      proccess: { error: { } }
    }
  ];
  

  id:number;
  data:any;

  constructor(
    private business:BusinessService,
    private unit:UnitService,
    private catcher:CatcherService,
  ) { }

  ngOnInit(): void {
    this.id = this.unit.currentId;
    this.reload();
  }

  setEdit(property) {
    property.editting = !property.editting;
    property.mirror = {
      [property.name]: this.data[property.name]
    };
  }

  async update(property) {
    if (!property.mirror) {
      return;
    }
    property.proccess = this.catcher.unitCreate(async () => {
      let enterprise = await this.business.updateEnterprise (this.id, property.mirror);
      property.editting = false;
      this.data = enterprise;
      return enterprise;
    });
    
    property.proccess.successToast = true;
  }

  async reload() {
    this.data = await this.business.getEnterprise (this.id);
  }

}
