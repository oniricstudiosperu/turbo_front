import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateFormComponent } from './create-form/create-form.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule, MatCommonModule } from '@angular/material/core';
import { ReactiveFormsModule } from '@angular/forms';
import { EnterpriseIntegraComponent } from './enterprise-integra/enterprise-integra.component';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { EnterpriseCoverComponent } from './enterprise-cover/enterprise-cover.component';
import { EnterpriseSubmenuComponent } from './enterprise-submenu/enterprise-submenu.component';



@NgModule({
  declarations: [
    CreateFormComponent,
    EnterpriseIntegraComponent,
    EnterpriseCoverComponent,
    EnterpriseSubmenuComponent
  ],
  imports: [
    CommonModule,
    MatCommonModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    NgbModule,
    RouterModule,
    MatMenuModule,
    MatButtonModule,
    
  ],
  exports: [
    CreateFormComponent,
    EnterpriseIntegraComponent,
    EnterpriseCoverComponent,
    EnterpriseSubmenuComponent
  ]
  
})
export class CompsModule { }
